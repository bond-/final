﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Data;
using System.Reflection;

namespace WpfApp1
{
    public class CBFile
    {
        public string Path { get; }
        public string Name { get; }
        public CBFile(string P, string N)
        {
            Path = P;
            Name = N;
        }
        public override string ToString()
        {
            return Name;
        }
    }
    public class Stats
    {
        public string Name { get; }
        public int TimeUsed { get; set; }
        public Stats(string File)
        {
            Name = File;
        }
        public Stats(string File, int time)
        {
            Name = File;
            TimeUsed = time;
        }
    }

    public partial class MainWindow : Window
    {
        private System.Windows.Forms.ContextMenu contextMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuItem3;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.NotifyIcon MyNotifyIcon;
        private System.IO.StreamWriter file;

        public List<CBFile> ListFileToHandle = new List<CBFile>();

        public System.Drawing.Size ClientSize { get; set; }
        public string Text { get; set; }
        public List<string> listItems = new List<string>();

        public List<CBFile> temp = new List<CBFile>(); // test

        public System.Windows.Controls.ComboBox CB = new System.Windows.Controls.ComboBox();
        public List<CBFile> Items = new List<CBFile>();
        public List<Stats> StatsList = new List<Stats>();

        private const string FILE_NAME = "save.txt";
        private const string FILE_STAT = "stats.txt";

        public MainWindow()
        {
            components = new System.ComponentModel.Container();
            contextMenu1 = new System.Windows.Forms.ContextMenu();
            menuItem1 = new System.Windows.Forms.MenuItem();
            menuItem2 = new System.Windows.Forms.MenuItem();
            menuItem3 = new System.Windows.Forms.MenuItem();


            CB = new System.Windows.Controls.ComboBox();
            contextMenu1.MenuItems.AddRange(
                    new System.Windows.Forms.MenuItem[] { menuItem1, menuItem2, menuItem3 });

            // Initialize menuItems
            menuItem1.Index = 0;
            menuItem1.Text = "Scan to build database";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);

            menuItem2.Index = 1;
            menuItem2.Text = "View Statitistics";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);

            menuItem3.Index = 2;
            menuItem3.Text = "Exit";
            this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);

                
            InitializeComponent();
            MyNotifyIcon = new System.Windows.Forms.NotifyIcon();
            MyNotifyIcon.Icon = new System.Drawing.Icon(@"icon.ico");
            MyNotifyIcon.Visible = true;
            MyNotifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(MyNotifyIcon_MouseDoubleClick);
            MyNotifyIcon.Text = "QuickLaunch";
            MyNotifyIcon.ContextMenu = contextMenu1;
            this.ShowInTaskbar = true;

            //load save file
            if (!File.Exists(FILE_NAME))
            {
                System.Windows.MessageBox.Show("Scanning to build database from C:\\Program Files", "First time run the app");
                ScanFile();
            }
            using (StreamReader sr = File.OpenText(FILE_NAME))
            {
                String input;
                while ((input = sr.ReadLine()) != null)
                {
                    listItems.Add(input);
                    string[] tokens = input.Split('|');
                    ListFileToHandle.Add(new CBFile(tokens[1], tokens[0]));
                }
                sr.Close();
            }
            if (!File.Exists(FILE_NAME))
            {
                using (StreamReader sr = File.OpenText(FILE_STAT))
                {
                    String statstr;
                    while ((statstr = sr.ReadLine()) != null)
                    {
                        string[] tokens = statstr.Split('|');
                        StatsList.Add(new Stats(tokens[0], Int32.Parse(tokens[1])));
                    }
                    sr.Close();
                }
            }
            // Add columns
            var gridView = new GridView();
            this.listView.View = gridView;
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Name",
                DisplayMemberBinding = new System.Windows.Data.Binding("Name")
            });
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Path",
                DisplayMemberBinding = new System.Windows.Data.Binding("Path")
            });

            // Populate list
            for (var i = 0; i < ListFileToHandle.Count(); i++)
            {
                temp.Add(new CBFile(ListFileToHandle[i].Path.ToString(), ListFileToHandle[i].Name.ToString()));
            }

            listView.ItemsSource = temp;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(listView.ItemsSource);
            view.Filter = UserFilter;
        }

        private void SearchDirectory(DirectoryInfo dir_info, List<string> file_list)
        {
            try
            {
                foreach (DirectoryInfo subdir_info in dir_info.GetDirectories())
                {
                    SearchDirectory(subdir_info, file_list);
                }
            }
            catch
            {
            }
            try
            {
                foreach (FileInfo file_info in dir_info.GetFiles("*.exe"))
                {
                    file_list.Add(file_info.FullName);
                }
            }
            catch
            {
            }
        }

        void ScanFile()
        {
            DirectoryInfo dir_info = new DirectoryInfo("C:\\Program Files");
            List<string> file_list = new List<string>();
            SearchDirectory(dir_info, file_list);

            int Max = 0;
            file = new System.IO.StreamWriter(@"save.txt");
            foreach (string F in file_list)
            {
                if (F.ToLower() == System.Windows.Forms.Application.ExecutablePath.ToLower()) continue;
                string FN = System.IO.Path.GetFileNameWithoutExtension(F);
                if (FN.Length > Max) Max = FN.Length;
                CB.Items.Add(new CBFile(F, FN));
                listItems.Add(FN.ToString());
                file.WriteLine(FN + "|" + F);
            }
            file.Close();
        }


        private void Run_Click(object sender, RoutedEventArgs e)
        {
            if (listView.SelectedIndex > -1)
            {
                dynamic PathFromCollum = listView.SelectedItem;
                if (listView.SelectedIndex > -1)
                {
                    var Path = PathFromCollum.Path;
                    Process p = Process.Start(Path);
                    string Name = PathFromCollum.Name;
                    if (StatsList.Exists(a => a.Name == Name) == false)
                    {
                        StatsList.Add(new Stats(PathFromCollum.Name, 1));
                    }
                    else
                        StatsList[StatsList.FindIndex(b => b.Name == Name)].TimeUsed = StatsList[StatsList.FindIndex(b => b.Name == Name)].TimeUsed + 1;
                }
                WriteStatsList();
            }
            else return;
        }

        private void HideToTaskBar_Click(object sender, EventArgs e)
        {
            WindowState = WindowState.Minimized;
            MyNotifyIcon.BalloonTipTitle = "Minimize Sucessful";
            MyNotifyIcon.BalloonTipText = "Minimized the app ";
            MyNotifyIcon.Visible = true;
            ShowInTaskbar = false;
        }

        private void Scan_Click(object sender, EventArgs e)
        {
            ScanFile();
        }

        private void Browse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.ShowDialog();
            AddFolderToList(folderBrowserDialog.SelectedPath.ToString());
        }

        void AddFolderToList(string path)
        {
            return;
        }

        void MyNotifyIcon_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            WindowState = WindowState.Normal;
            ShowInTaskbar = true;
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            if (string.Equals((sender as System.Windows.Forms.Button).Name, @"CloseButton"))
            {
                MyNotifyIcon.BalloonTipTitle = "Minimize Sucessful";
                MyNotifyIcon.BalloonTipText = "Minimized the app ";
                MyNotifyIcon.Visible = true;
            }
            if (this.WindowState == System.Windows.WindowState.Maximized)
            {
                this.ShowInTaskbar = false;
                MyNotifyIcon.BalloonTipTitle = "Minimize Sucessful";
                MyNotifyIcon.BalloonTipText = "Minimized the app ";
                MyNotifyIcon.Visible = true;
            }
            else if (WindowState == WindowState.Normal)
            {
                this.MyNotifyIcon.Visible = true;
                this.ShowInTaskbar = true;
            }
        }

        private void listView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void listView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Controls.ListView lv = (System.Windows.Controls.ListView)sender;
            dynamic PathFromCollum = lv.SelectedItem;
            if (lv.SelectedIndex > -1)
            {
                var Path = PathFromCollum.Path;
                Process p = Process.Start(Path);
                string Name = PathFromCollum.Name;
                if (StatsList.Exists(a => a.Name == Name) == false)
                {
                    StatsList.Add(new Stats(PathFromCollum.Name, 1));
                }
                else
                    StatsList[StatsList.FindIndex(b => b.Name == Name)].TimeUsed = StatsList[StatsList.FindIndex(b => b.Name == Name)].TimeUsed + 1;
            }
            WriteStatsList();
        }

        private void WriteStatsList()
        {
            file = new System.IO.StreamWriter(@"stats.txt");
            StatsList.OrderBy(p => p.TimeUsed);
            foreach (Stats F in StatsList)
            {
                file.WriteLine(F.Name + "|" + F.TimeUsed);
            }

            file.Close();
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(textBox.Text))
                return true;
            else
                return ((item as CBFile).Name.IndexOf(textBox.Text, StringComparison.OrdinalIgnoreCase) >= 0);
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(listView.ItemsSource).Refresh();
        }

        private void RemoveContextMenu_OnClick(object sender, RoutedEventArgs e)
        {
            temp.RemoveAt(listView.SelectedIndex);
            listView.ItemsSource = temp;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(listView.ItemsSource);
            view.Filter = UserFilter;
            CollectionViewSource.GetDefaultView(listView.ItemsSource).Refresh();
            //rewrite files
            int Max = 0;
            file = new System.IO.StreamWriter(@"save.txt");
            foreach (CBFile F in temp)
            {
                if (F.Name.ToLower() == System.Windows.Forms.Application.ExecutablePath.ToLower()) continue;
                string FN = F.Name;
                if (FN.Length > Max) Max = FN.Length;
                file.WriteLine(FN + "|" + F.Path);
            }
            file.Close();
        }

        private void RunContextMenu_OnClick(object sender, RoutedEventArgs e)
        {
            dynamic PathFromCollum = listView.SelectedItem;
            if (listView.SelectedIndex > -1)
            {
                var Path = PathFromCollum.Path;
                Process p = Process.Start(Path);
                string Name = PathFromCollum.Name;
                if (StatsList.Exists(a => a.Name == Name) == false)
                {
                    StatsList.Add(new Stats(PathFromCollum.Name, 1));
                }
                else
                    StatsList[StatsList.FindIndex(b => b.Name == Name)].TimeUsed = StatsList[StatsList.FindIndex(b => b.Name == Name)].TimeUsed + 1;
            }
            WriteStatsList();
        }

        private void listView_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (listView.SelectedIndex > -1)
                {
                    dynamic PathFromCollum = listView.SelectedItem;
                    var Path = PathFromCollum.Path;
                    Process p = Process.Start(Path);
                    string Name = PathFromCollum.Name;
                    if (StatsList.Exists(a => a.Name == Name) == false)
                    {
                        StatsList.Add(new Stats(PathFromCollum.Name, 1));
                    }
                    else
                        StatsList[StatsList.FindIndex(b => b.Name == Name)].TimeUsed = StatsList[StatsList.FindIndex(b => b.Name == Name)].TimeUsed + 1;
                }
                WriteStatsList();
            }
            else return;

        }

        public void menuItem1_Click(object sender, System.EventArgs e)
        {
            ScanFile();
            CollectionViewSource.GetDefaultView(listView.ItemsSource).Refresh();
        }
        public void menuItem2_Click(object sender, System.EventArgs e)
        {
        }
        public void menuItem3_Click(object sender, System.EventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }
    }
}
