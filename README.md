#QuickLaunch by 1512492 - Final

##Họ tên: Nguyễn Xuân Tân     MSSV:1512492


###Tính năng đã làm được:

1. Chạy phần mềm, một notification icon sẽ được mở ở vùng thông báo của máy

2. Right click để chọn tính năng: Scan to build database (C:\Program Files), View statitistics, Exit

3. Exit để đóng ứng dụng

4. Chạy ứng dụng bằng cách: Double click, nhấn Run, chọn bằng mũi tên rồi nhấn Enter

5. Có thể tùy chỉnh file bằng cách chuột phải -> remove

6. Lưu database tên: save.txt, lưu tần suất sử dụng trong stats.txt, mỗi lần chạy tăng timesused lên 1 đơn vị

7. Gõ một chữ, sẽ tìm được chương trình cần tìm theo tên


####Link bitbucket: https://bitbucket.org/bond-/final
####Link youtube: https://www.youtube.com/watch?v=4ySTOkcxyAQ